package com.devcamp.productlistapi.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.productlistapi.model.CProduct;

public interface IProductRepository extends JpaRepository<CProduct, Long> {
    
}

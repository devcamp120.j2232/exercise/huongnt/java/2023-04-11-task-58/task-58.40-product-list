package com.devcamp.productlistapi.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.productlistapi.model.CProduct;
import com.devcamp.productlistapi.repository.IProductRepository;



@CrossOrigin
@RestController
@RequestMapping("/")

public class CProductController {
    @Autowired
    IProductRepository pProductRepository;

    @GetMapping("/products")
    public ResponseEntity<List<CProduct>> getAllProducts(){
        try{
            List<CProduct> listProduct = new ArrayList<CProduct>();

            pProductRepository.findAll()
            .forEach(listProduct::add);
            return new ResponseEntity<>(listProduct, HttpStatus.OK);

        } catch (Exception ex) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        
    }
}

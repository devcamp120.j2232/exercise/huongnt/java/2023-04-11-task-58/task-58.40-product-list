package com.devcamp.productlistapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProductlistapiApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProductlistapiApplication.class, args);
	}

}
